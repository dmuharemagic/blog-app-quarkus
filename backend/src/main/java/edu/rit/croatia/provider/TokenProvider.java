package edu.rit.croatia.provider;

import edu.rit.croatia.domain.Role;
import edu.rit.croatia.domain.User;
import io.smallrye.jwt.build.Jwt;
import io.smallrye.jwt.build.JwtClaimsBuilder;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.stream.Collectors;

public class TokenProvider {

    private static final long EXPIRES_IN_SECONDS = 86400;

    private TokenProvider() {

    }

    public static String generateTokenString(User user) throws Exception {
        String kid = "/private_key.pem";
        PrivateKey pk = readPrivateKey(kid);

        JwtClaimsBuilder claims = Jwt.claims();
        claims.issuer("dino.muharemagic");
        claims.upn(user.getUsername());
        claims.groups(user.getRoles().stream()
                .map(Role::getRole)
                .collect(Collectors.toSet()));
        claims.claim("userId", user.getId());

        long currentTimeInSecs = currentTimeInSecs();
        long exp = currentTimeInSecs + EXPIRES_IN_SECONDS;

        claims.issuedAt(currentTimeInSecs);
        claims.claim(Claims.auth_time.name(), currentTimeInSecs);
        claims.expiresAt(exp);

        return claims.jws().signatureKeyId(kid).sign(pk);
    }

    public static String refreshToken(JsonWebToken token) throws Exception {
        String kid = "/private_key.pem";
        PrivateKey pk = readPrivateKey(kid);

        JwtClaimsBuilder claims = Jwt.claims();
        claims.issuer("dino.muharemagic");
        claims.upn(token.getName());
        claims.groups(token.getGroups());

        long currentTimeInSecs = currentTimeInSecs();
        long exp = currentTimeInSecs + EXPIRES_IN_SECONDS;

        claims.issuedAt(currentTimeInSecs);
        claims.claim(Claims.auth_time.name(), currentTimeInSecs);
        claims.expiresAt(exp);

        return claims.jws().signatureKeyId(kid).sign(pk);
    }

    private static PrivateKey readPrivateKey(final String pemResName) throws Exception {
        try (InputStream contentIS = TokenProvider.class.getResourceAsStream(pemResName)) {
            byte[] tmp = new byte[4096];
            int length = contentIS.read(tmp);
            return decodePrivateKey(new String(tmp, 0, length, StandardCharsets.UTF_8));
        }
    }

    private static PrivateKey decodePrivateKey(final String pemEncoded) throws Exception {
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(toEncodedBytes(pemEncoded));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpec);
    }

    private static byte[] toEncodedBytes(final String pemEncoded) {
        return Base64.getDecoder().decode(removeBeginEnd(pemEncoded));
    }

    private static String removeBeginEnd(String pem) {
        pem = pem.replaceAll("-----BEGIN (.*)-----", "");
        pem = pem.replaceAll("-----END (.*)----", "");
        pem = pem.replaceAll("\r\n", "");
        pem = pem.replaceAll("\n", "");
        return pem.trim();
    }

    public static int currentTimeInSecs() {
        return (int) (System.currentTimeMillis() / 1000);
    }
}