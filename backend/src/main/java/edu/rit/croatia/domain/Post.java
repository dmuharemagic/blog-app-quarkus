package edu.rit.croatia.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Data
@EqualsAndHashCode
@Entity
@org.hibernate.annotations.Cache(
        usage = CacheConcurrencyStrategy.READ_WRITE
)
@NaturalIdCache
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "content", nullable = false)
    private String content;

    @NaturalId
    @Column(name = "slug", nullable = false)
    private String slug;

    @Column(name = "category_id", nullable = false)
    private Long categoryId;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "is_deleted", nullable = false)
    private boolean isDeleted;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "post", orphanRemoval = true)
    @Where(clause = "parent_id IS NULL")
    private Set<Comment> comments = new HashSet<>();

    @Formula("(SELECT COUNT(*) FROM comment c WHERE c.post_id = id)")
    private int commentCount;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "post", orphanRemoval = true)
    private Set<PostLike> likes = new HashSet<>();

    @Formula("(SELECT COUNT(*) FROM post_like p WHERE p.post_id = id)")
    private int likeCount;

    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Category category;

    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private User user;

    @EqualsAndHashCode.Exclude
    @OneToMany(
            mappedBy = "post",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<PostMedia> media = new ArrayList<>();

    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "post_tag",
            joinColumns = @JoinColumn(name = "post_id", nullable = false, updatable = false),
            inverseJoinColumns = @JoinColumn(name = "tag_id", nullable = false, updatable = false)
    )
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "tagcache")
    private Set<Tag> tags = new HashSet<>();

    @EqualsAndHashCode.Exclude
    @OneToOne(mappedBy = "post")
    private PostView views;

    @Formula("(SELECT COUNT(*) FROM post_view p WHERE p.post_id = id)")
    private int viewCount;

    public void addMedia(Media mediaItem) {
        PostMedia postMedia = new PostMedia(this, mediaItem);
        media.add(postMedia);
        mediaItem.getPosts().add(postMedia);
    }

    public void removeMedia(Media mediaItem) {
        Iterator<PostMedia> iterator = media.iterator();

        while (iterator.hasNext()) {
            PostMedia postMedia = iterator.next();

            if (postMedia.getPost().equals(this) && postMedia.getMedia().equals(mediaItem)) {
                iterator.remove();
                postMedia.getMedia().getPosts().remove(postMedia);
                postMedia.setPost(null);
                postMedia.setMedia(null);
            }
        }
    }
}
