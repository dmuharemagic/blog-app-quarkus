package edu.rit.croatia.domain;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "post_view", schema = "blog")
public class PostView {

    @Id
    @NonNull
    @Column(name = "post_id", nullable = false)
    private Long postId;

    @NonNull
    @Column(name = "count")
    private int count;

    @NonNull
    @Column(name = "last_viewed", nullable = false)
    private LocalDateTime lastViewed;

    @EqualsAndHashCode.Exclude
    @OneToOne
    @JoinColumn(name = "post_id", referencedColumnName = "id", nullable = false)
    private Post post;

    public PostView increment() {
        this.count = ++this.count;
        return this;
    }

}
