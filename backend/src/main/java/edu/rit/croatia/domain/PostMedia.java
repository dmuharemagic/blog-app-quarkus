package edu.rit.croatia.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "post_media", schema = "blog")
public class PostMedia {

    @EmbeddedId
    private PostMediaPK id;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("postId")
    private Post post;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("mediaId")
    private Media media;

    public PostMedia(Post post, Media media) {
        this.id = new PostMediaPK(post.getId(), media.getId());
        this.post = post;
        this.media = media;
    }
}
