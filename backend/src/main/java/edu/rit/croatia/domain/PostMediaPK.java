package edu.rit.croatia.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@EqualsAndHashCode
@Embeddable
public class PostMediaPK implements Serializable {

    @Column(name = "post_id", nullable = false)
    private Long postId;

    @Column(name = "media_id", nullable = false)
    private Long mediaId;

    public PostMediaPK(Long postId, Long mediaId) {
        this.postId = postId;
        this.mediaId = mediaId;
    }

}
