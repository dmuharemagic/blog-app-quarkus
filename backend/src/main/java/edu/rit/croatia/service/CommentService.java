package edu.rit.croatia.service;

import edu.rit.croatia.domain.Comment;
import edu.rit.croatia.domain.CommentLike;
import edu.rit.croatia.exception.IncorrectUserCommentException;
import edu.rit.croatia.exception.NotFoundException;
import edu.rit.croatia.repository.CommentLikeRepository;
import edu.rit.croatia.repository.CommentRepository;
import edu.rit.croatia.repository.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class CommentService {

    private final CommentRepository commentRepository;
    private final CommentLikeRepository likeRepository;
    private final UserRepository userRepository;

    @Inject
    public CommentService(CommentRepository commentRepository, CommentLikeRepository likeRepository, UserRepository userRepository) {
        this.commentRepository = commentRepository;
        this.likeRepository = likeRepository;
        this.userRepository = userRepository;
    }

    public List<Comment> getAll() {
        return this.commentRepository.findAll().list();
    }

    public Comment get(long id) {
        return this.commentRepository.findByIdOptional(id).orElseThrow(() -> new NotFoundException("Unable to find a comment with an ID of " + id + "."));
    }

    @Transactional
    public Comment add(Comment comment) {
        var userId = comment.getUserId();
        var user = userRepository.findByIdOptional(userId).orElseThrow(() -> new NotFoundException("Unable to find a user with an ID of " + userId + "."));

        comment.setUser(user);

        return this.commentRepository.save(comment);
    }

    @Transactional
    public void delete(long id, String username) {
        var comment = this.commentRepository.findByIdOptional(id)
                .orElseThrow(() -> new NotFoundException("Unable to find a comment with an ID of " + id + "."));
        var user = this.userRepository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException("Incorrect user ID."));

        if (user.getId().equals(comment.getUserId()))
            comment.setDeleted(true);
        else
            throw new IncorrectUserCommentException();

        this.commentRepository.persist(comment);
    }

    @Transactional
    public boolean getVote(long id, String username) {
        var user = this.userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException("Incorrect user ID."));

        if (!this.commentRepository.existsBy(Map.of("id", id))) {
            throw new NotFoundException("Incorrect comment ID.");
        }

        return this.likeRepository.existsBy(Map.of("user_id", user.getId(), "comment_id", id));
    }

    @Transactional
    public void castVote(long id, String username) {
        var user = this.userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException("Incorrect user ID."));
        var comment = this.commentRepository.findByIdOptional(id).orElseThrow(() -> new NotFoundException("Incorrect comment ID."));

        likeRepository.findByUserAndComment(user.getId(), comment.getId())
                .ifPresentOrElse(likeRepository::delete, () -> likeRepository.save(new CommentLike(comment, user)));
    }
}