package edu.rit.croatia.service;

import edu.rit.croatia.domain.Tag;
import edu.rit.croatia.exception.NotFoundException;
import edu.rit.croatia.repository.TagRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class TagService {

    private final TagRepository repository;

    @Inject
    public TagService(TagRepository repository) {
        this.repository = repository;
    }

    public List<Tag> getAll() {
        return this.repository.findAll().list();
    }

    public Tag get(String slug) {
        return this.repository.findBySlug(slug).orElseThrow(() -> new NotFoundException("Unable to find a tag with an ID of " + slug + "."));
    }

    @Transactional
    public void delete(long id) {
        var tag = this.repository.findByIdOptional(id).orElseThrow(() -> new NotFoundException("Unable to find a tag with an ID of " + id + "."));
        this.repository.delete(tag);
    }
    
}