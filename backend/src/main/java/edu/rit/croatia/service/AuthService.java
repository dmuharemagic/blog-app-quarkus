package edu.rit.croatia.service;

import edu.rit.croatia.domain.User;
import edu.rit.croatia.exception.EmailAlreadyExistsException;
import edu.rit.croatia.exception.IncorrectLoginException;
import edu.rit.croatia.exception.UsernameAlreadyExistsException;
import edu.rit.croatia.repository.RoleRepository;
import edu.rit.croatia.repository.UserRepository;
import org.mindrot.jbcrypt.BCrypt;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Map;

@ApplicationScoped
public class AuthService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Inject
    public AuthService(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Transactional
    public void register(User user) {
        if (userRepository.existsBy(Map.of("username", user.getUsername()))) {
            throw new UsernameAlreadyExistsException();
        }

        if (userRepository.existsBy(Map.of("email", user.getEmail()))) {
            throw new EmailAlreadyExistsException();
        }

        user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
        user.addRole(roleRepository.findByName("User"));

        this.userRepository.save(user);
    }

    public User authenticate(User user) {
        var found = userRepository.findByUsername(user.getUsername()).orElseThrow(IncorrectLoginException::new);

        if (!BCrypt.checkpw(user.getPassword(), found.getPassword())) {
            throw new IncorrectLoginException();
        }

        return found;
    }

}
