package edu.rit.croatia.service;

import edu.rit.croatia.PageRequest;
import edu.rit.croatia.domain.User;
import edu.rit.croatia.repository.UserRepository;
import io.quarkus.panache.common.Page;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.NotFoundException;
import java.util.List;

@ApplicationScoped
public class UserService {

    private final UserRepository repository;

    @Inject
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public List<User> getAll(@BeanParam PageRequest request) {
        return this.repository.findAll()
                .page(Page.of(request.getPage(), request.getSize()))
                .list();
    }

    public User get(long id) {
        return this.repository.findByIdOptional(id).orElseThrow(() -> new NotFoundException("Unable to find the user with a given ID."));
    }

}
