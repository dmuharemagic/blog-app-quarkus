package edu.rit.croatia.service;

import edu.rit.croatia.PageRequest;
import edu.rit.croatia.domain.Post;
import edu.rit.croatia.domain.PostLike;
import edu.rit.croatia.domain.PostView;
import edu.rit.croatia.domain.Tag;
import edu.rit.croatia.dto.request.PostRequestDTO;
import edu.rit.croatia.exception.NotFoundException;
import edu.rit.croatia.exception.PostAlreadyExistsException;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import edu.rit.croatia.mapper.request.PostRequestMapper;
import edu.rit.croatia.repository.*;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BeanParam;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class PostService {

    private final UserRepository userRepository;

    private final CategoryRepository categoryRepository;

    private final PostRepository postRepository;
    private final PostLikeRepository likeRepository;
    private final PostViewRepository postViewRepository;

    private final TagRepository tagRepository;

    private final PostRequestMapper postRequestMapper;

    @Inject
    public PostService(UserRepository userRepository, PostLikeRepository likeRepository, PostRepository postRepository, PostViewRepository postViewRepository, CategoryRepository categoryRepository, TagRepository tagRepository, PostRequestMapper postRequestMapper) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.likeRepository = likeRepository;
        this.postViewRepository = postViewRepository;
        this.categoryRepository = categoryRepository;
        this.tagRepository = tagRepository;
        this.postRequestMapper = postRequestMapper;
    }

    public List<Post> getAll(@BeanParam PageRequest request) {
        return this.postRepository.findAll(Sort.by("created_at").descending())
                .page(Page.of(request.getPage(), request.getSize()))
                .list();
    }

    public Post get(long id, String slug) {
        return this.postRepository.findBySlug(id, slug)
                .orElseThrow(() -> new NotFoundException("Unable to find a post with an ID of " + id + " and slug " + slug + "."));
    }

    @Transactional
    public Post add(PostRequestDTO request) {
        var userId = request.getUserId();
        var categoryId = request.getCategoryId();
        var slug = request.getSlug();

        if (!userRepository.existsBy(Map.of("id", userId))) {
            throw new NotFoundException("Unable to find a user with an ID of " + userId + ".");
        }

        if (!categoryRepository.existsBy(Map.of("id", categoryId))) {
            throw new NotFoundException("Unable to find a category with an ID of " + categoryId + ".");
        }

        if (postRepository.existsBy(Map.of("slug", slug))) {
            throw new PostAlreadyExistsException();
        }

        Set<Tag> tags = request.getTags().stream()
                .map(tag -> tagRepository.findBySlug(tag.getSlug())
                        .orElseGet(() -> tagRepository.save(new Tag(tag.getSlug()))))
                .collect(Collectors.toUnmodifiableSet());

        return this.postRepository.save(postRequestMapper.toDomain(request, tags, new CycleAvoidingMappingContext()));
    }

    @Transactional
    public boolean hasVoted(long id, String slug, String username) {
        var user = this.userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException("Incorrect user ID specified."));
        var post = this.postRepository.existsBy(Map.of("slug", slug));

        if (!post) {
            throw new NotFoundException("Incorrect post ID specified.");
        }

        return this.likeRepository.findByUserAndPost(user.getId(), id).isPresent();
    }

    @Transactional
    public void castVote(long id, String slug, String username) {
        var user = this.userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException("Incorrect user ID specified."));
        var post = this.postRepository.findBySlug(id, slug).orElseThrow(() -> new NotFoundException("Incorrect post ID and slug specified."));

        likeRepository.findByUserAndPost(user.getId(), post.getId())
                .ifPresentOrElse(likeRepository::delete, () -> likeRepository.save(new PostLike(post, user)));
    }

    public PostView getViews(long id) {
        return postViewRepository.findByPostId(id).orElseGet(() -> new PostView(id, 0, LocalDateTime.now()));
    }

    @Transactional
    public PostView updateViewCount(long id) {
        return postViewRepository.findByPostId(id)
                .map(views -> postViewRepository.save(views.increment()))
                .orElseGet(() -> postViewRepository.save(new PostView(id, 1, LocalDateTime.now())));
    }
}
