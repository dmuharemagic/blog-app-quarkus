package edu.rit.croatia.service;

import edu.rit.croatia.domain.Category;
import edu.rit.croatia.exception.NotFoundException;
import edu.rit.croatia.repository.CategoryRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class CategoryService {

    private final CategoryRepository repository;

    @Inject
    public CategoryService(CategoryRepository repository) {
        this.repository = repository;
    }

    public List<Category> getAll() {
        return this.repository.findAll().list();
    }

    public Category get(long id) {
        return this.repository.findByIdOptional(id).orElseThrow(() -> new NotFoundException("Unable to find a category with an ID of " + id + "."));
    }

    @Transactional
    public void delete(long id) {
        var category = this.repository.findByIdOptional(id).orElseThrow(() -> new NotFoundException("Unable to find a category with an ID of " + id + "."));
        this.repository.delete(category);
    }

}