package edu.rit.croatia.mapper;

import edu.rit.croatia.domain.Category;
import edu.rit.croatia.dto.CategoryDTO;
import edu.rit.croatia.mapper.config.BlogMappingConfig;
import edu.rit.croatia.mapper.config.CycleAvoidingEntityMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import org.mapstruct.*;

import java.util.List;

@Mapper(config = BlogMappingConfig.class)
public interface CategoryMapper extends CycleAvoidingEntityMapper<CategoryDTO, Category> {

    @IterableMapping(qualifiedByName = "categories")
    List<CategoryDTO> toListResource(List<Category> entities, @Context CycleAvoidingMappingContext context);

    @Named("categories")
    @Mappings({
            @Mapping(target = "posts", ignore = true),
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true),
    })
    CategoryDTO toResource(Category entity, @Context CycleAvoidingMappingContext context);

}
