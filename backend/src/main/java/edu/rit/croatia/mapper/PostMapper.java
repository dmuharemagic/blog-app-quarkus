package edu.rit.croatia.mapper;

import edu.rit.croatia.domain.Post;
import edu.rit.croatia.dto.PostDTO;
import edu.rit.croatia.mapper.config.BlogMappingConfig;
import edu.rit.croatia.mapper.config.CycleAvoidingEntityMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import org.mapstruct.*;

import java.util.List;

@Mapper(config = BlogMappingConfig.class)
public interface PostMapper extends CycleAvoidingEntityMapper<PostDTO, Post> {

    @IterableMapping(qualifiedByName = "posts")
    List<PostDTO> toListResource(List<Post> entities, @Context CycleAvoidingMappingContext context);

    @Named("posts")
    @Mappings({
            @Mapping(target = "comments", ignore = true),
            @Mapping(target = "media", ignore = true),
    })
    PostDTO toResource(Post entity, @Context CycleAvoidingMappingContext context);

}
