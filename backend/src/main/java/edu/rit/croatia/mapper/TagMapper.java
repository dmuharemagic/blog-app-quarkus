package edu.rit.croatia.mapper;

import edu.rit.croatia.domain.Tag;
import edu.rit.croatia.dto.TagDTO;
import edu.rit.croatia.mapper.config.BlogMappingConfig;
import edu.rit.croatia.mapper.config.CycleAvoidingEntityMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import org.mapstruct.Context;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;

@Mapper(config = BlogMappingConfig.class)
public interface TagMapper extends CycleAvoidingEntityMapper<TagDTO, Tag> {

    @IterableMapping(qualifiedByName = "tags")
    List<TagDTO> toListResource(List<Tag> entities, @Context CycleAvoidingMappingContext context);

    @Named("tags")
    TagDTO toResource(Tag entity, @Context CycleAvoidingMappingContext context);

}
