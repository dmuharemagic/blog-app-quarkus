package edu.rit.croatia.mapper;

import edu.rit.croatia.domain.User;
import edu.rit.croatia.dto.UserDTO;
import edu.rit.croatia.mapper.config.BlogMappingConfig;
import edu.rit.croatia.mapper.config.CycleAvoidingEntityMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import org.mapstruct.*;

import java.util.List;

@Mapper(config = BlogMappingConfig.class)
public interface UserMapper extends CycleAvoidingEntityMapper<UserDTO, User> {

    @IterableMapping(qualifiedByName = "users")
    List<UserDTO> toListResource(List<User> entities, @Context CycleAvoidingMappingContext context);

    @Named("users")
    @Mappings({
            @Mapping(target = "posts", ignore = true),
            @Mapping(target = "comments", ignore = true)
    })
    UserDTO toResource(User entity, @Context CycleAvoidingMappingContext context);

}
