package edu.rit.croatia.mapper;

import edu.rit.croatia.domain.Post;
import edu.rit.croatia.dto.PostDTO;
import edu.rit.croatia.mapper.config.BlogMappingConfig;
import edu.rit.croatia.mapper.config.CycleAvoidingEntityMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import org.mapstruct.Context;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;

@Mapper(config = BlogMappingConfig.class)
public interface PostDetailMapper extends CycleAvoidingEntityMapper<PostDTO, Post> {

    @IterableMapping(qualifiedByName = "postDetails")
    List<PostDTO> toListResource(List<Post> entities, @Context CycleAvoidingMappingContext context);

    @Named("postDetails")
    PostDTO toResource(Post entity, @Context CycleAvoidingMappingContext context);

}
