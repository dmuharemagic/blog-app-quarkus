package edu.rit.croatia.mapper.request;

import edu.rit.croatia.domain.Comment;
import edu.rit.croatia.dto.request.CommentRequestDTO;
import edu.rit.croatia.mapper.config.BlogMappingConfig;
import edu.rit.croatia.mapper.config.CycleAvoidingEntityMapper;
import org.mapstruct.Mapper;

@Mapper(config = BlogMappingConfig.class)
public interface CommentRequestMapper extends CycleAvoidingEntityMapper<CommentRequestDTO, Comment> {

}
