package edu.rit.croatia.mapper.request;

import edu.rit.croatia.domain.Post;
import edu.rit.croatia.domain.Tag;
import edu.rit.croatia.dto.request.PostRequestDTO;
import edu.rit.croatia.mapper.config.BlogMappingConfig;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.Set;

@Mapper(config = BlogMappingConfig.class)
public interface PostRequestMapper {

    List<Post> toDomain(List<PostRequestDTO> dto, @Context CycleAvoidingMappingContext context);

    @Mapping(source = "tagSet", target = "tags")
    Post toDomain(PostRequestDTO dto, Set<Tag> tagSet, @Context CycleAvoidingMappingContext context);

}
