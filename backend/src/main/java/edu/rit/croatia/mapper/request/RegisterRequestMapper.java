package edu.rit.croatia.mapper.request;

import edu.rit.croatia.domain.User;
import edu.rit.croatia.dto.request.RegisterRequestDTO;
import edu.rit.croatia.mapper.config.BlogMappingConfig;
import edu.rit.croatia.mapper.config.CycleAvoidingEntityMapper;
import org.mapstruct.Mapper;

@Mapper(config = BlogMappingConfig.class)
public interface RegisterRequestMapper extends CycleAvoidingEntityMapper<RegisterRequestDTO, User> {

}
