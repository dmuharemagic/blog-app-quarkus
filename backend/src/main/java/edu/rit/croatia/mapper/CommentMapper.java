package edu.rit.croatia.mapper;

import edu.rit.croatia.domain.Comment;
import edu.rit.croatia.dto.CommentDTO;
import edu.rit.croatia.mapper.config.BlogMappingConfig;
import edu.rit.croatia.mapper.config.CycleAvoidingEntityMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import org.mapstruct.Context;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;

@Mapper(config = BlogMappingConfig.class)
public interface CommentMapper extends CycleAvoidingEntityMapper<CommentDTO, Comment> {

    @IterableMapping(qualifiedByName = "comment")
    List<CommentDTO> toListResource(List<Comment> entities, @Context CycleAvoidingMappingContext context);

    @Named("comment")
    CommentDTO toResource(Comment entity, @Context CycleAvoidingMappingContext context);

}
