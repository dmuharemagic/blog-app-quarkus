package edu.rit.croatia.mapper.config;

import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import org.mapstruct.Context;

import java.util.List;

public interface CycleAvoidingEntityMapper<D, E> {

    List<E> toDomain(List<D> dto, @Context CycleAvoidingMappingContext context);

    E toDomain(D dto, @Context CycleAvoidingMappingContext context);

}

