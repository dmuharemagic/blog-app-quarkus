package edu.rit.croatia.repository;

import edu.rit.croatia.domain.Post;
import edu.rit.croatia.repository.base.ExtendedPanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class PostRepository extends ExtendedPanacheRepository<Post> {

    public PostRepository() {
        super("post");
    }

    public Optional<Post> findBySlug(long id, String slug) {
        return find("id = ?1 and slug = ?2", id, slug).firstResultOptional();
    }

}
