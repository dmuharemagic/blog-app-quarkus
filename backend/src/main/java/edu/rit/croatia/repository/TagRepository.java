package edu.rit.croatia.repository;

import edu.rit.croatia.domain.Tag;
import edu.rit.croatia.repository.base.ExtendedPanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class TagRepository extends ExtendedPanacheRepository<Tag> {

    public TagRepository() {
        super("tag");
    }

    public Optional<Tag> findBySlug(String slug) {
        return find("slug", slug).firstResultOptional();
    }

}
