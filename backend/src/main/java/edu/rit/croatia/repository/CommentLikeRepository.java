package edu.rit.croatia.repository;

import edu.rit.croatia.domain.CommentLike;
import edu.rit.croatia.repository.base.ExtendedPanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class CommentLikeRepository extends ExtendedPanacheRepository<CommentLike> {

    public CommentLikeRepository() {
        super("comment_like");
    }

    public Optional<CommentLike> findByUserAndComment(long userId, long commentId) {
        return find("user_id = ?1 and comment_id = ?2", userId, commentId).firstResultOptional();
    }

}
