package edu.rit.croatia.repository;

import edu.rit.croatia.domain.Category;
import edu.rit.croatia.repository.base.ExtendedPanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CategoryRepository extends ExtendedPanacheRepository<Category> {

    public CategoryRepository() {
        super("category");
    }
}
