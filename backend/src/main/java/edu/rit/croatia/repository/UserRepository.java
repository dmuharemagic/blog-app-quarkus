package edu.rit.croatia.repository;

import edu.rit.croatia.domain.User;
import edu.rit.croatia.repository.base.ExtendedPanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class UserRepository extends ExtendedPanacheRepository<User> {

    public UserRepository() {
        super("user");
    }

    public Optional<User> findByUsername(String username) {
        return find("username", username).firstResultOptional();
    }

}
