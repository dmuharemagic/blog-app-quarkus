package edu.rit.croatia.repository.base;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.hibernate.orm.panache.runtime.JpaOperations;
import lombok.NoArgsConstructor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.Map;
import java.util.stream.Collectors;

@Named
@ApplicationScoped
@NoArgsConstructor
public class ExtendedPanacheRepository<Entity> implements PanacheRepository<Entity> {

    private static final String EQUALS_CONDITION_STRING = "%s.%s = :%s";
    private static final String EXISTS_QUERY_STRING = "SELECT EXISTS (SELECT 1 FROM ";

    String table;

    public ExtendedPanacheRepository(String table) {
        this.table = table;
    }
    
    public boolean existsBy(Map<String, Object> attributes) {
        String whereClause = attributes.keySet().stream()
                .map(key -> String.format(EQUALS_CONDITION_STRING, table, key, key))
                .collect(Collectors.joining(" AND ", " WHERE ", ""));

        String queryString = EXISTS_QUERY_STRING + table + whereClause + ")";

        Query query = JpaOperations.getEntityManager().createNativeQuery(queryString);

        attributes.forEach(query::setParameter);

        final BigInteger result = (BigInteger) query.getSingleResult();
        return result != null && result.longValue() > 0;
    }

    @Transactional
    public Entity save(Entity entity) {
        EntityManager em = JpaOperations.getEntityManager();
        if (!isPersistent(entity)) {
            em.persist(entity);
            return entity;
        } else {
            return em.merge(entity);
        }
    }

    public void refresh(Entity entity) {
        JpaOperations.getEntityManager().refresh(entity);
    }

}
