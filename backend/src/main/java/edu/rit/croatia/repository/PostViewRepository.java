package edu.rit.croatia.repository;

import edu.rit.croatia.domain.PostView;
import edu.rit.croatia.repository.base.ExtendedPanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class PostViewRepository extends ExtendedPanacheRepository<PostView> {

    public PostViewRepository() {
        super("post_view");
    }

    public Optional<PostView> findByPostId(long id) {
        return find("post_id", id).firstResultOptional();
    }

}
