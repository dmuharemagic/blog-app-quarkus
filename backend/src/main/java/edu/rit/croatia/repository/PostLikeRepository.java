package edu.rit.croatia.repository;

import edu.rit.croatia.domain.PostLike;
import edu.rit.croatia.repository.base.ExtendedPanacheRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class PostLikeRepository extends ExtendedPanacheRepository<PostLike> {

    public PostLikeRepository() {
        super("post_like");
    }

    public Optional<PostLike> findByUserAndPost(long userId, long postId) {
        return find("user_id = ?1 and post_id = ?2", userId, postId).firstResultOptional();
    }

}
