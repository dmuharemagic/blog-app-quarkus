package edu.rit.croatia.repository;

import edu.rit.croatia.domain.Comment;
import edu.rit.croatia.repository.base.ExtendedPanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CommentRepository extends ExtendedPanacheRepository<Comment> {

    public CommentRepository() {
        super("comment");
    }
}
