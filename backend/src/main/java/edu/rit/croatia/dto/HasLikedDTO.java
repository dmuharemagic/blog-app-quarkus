package edu.rit.croatia.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HasLikedDTO {
    private boolean liked;
}
