package edu.rit.croatia.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class CategoryRequestDTO {

    @Size(min = 5, max = 255, message = "Category title must be between 5 and 255 characters.")
    @NotBlank(message = "Category title must not be an empty field.")
    private String title;

}