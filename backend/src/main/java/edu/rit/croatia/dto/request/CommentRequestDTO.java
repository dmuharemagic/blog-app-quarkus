package edu.rit.croatia.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CommentRequestDTO {

    @NotBlank(message = "Body must not be an empty field.")
    private String body;

    private Long parentId;

    @NotNull(message = "Post ID must not be empty.")
    private Long postId;

    @NotNull(message = "User ID must not be empty.")
    private Long userId;

}