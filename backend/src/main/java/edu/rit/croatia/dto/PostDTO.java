package edu.rit.croatia.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@JsonPropertyOrder({"id", "title", "content", "slug", "views", "likes", "comment_count"})
public class PostDTO {

    private long id;
    private String title;
    private String content;
    private String slug;

    @JsonProperty("views")
    private int viewCount;

    @JsonProperty("likes")
    private int likeCount;

    @JsonProperty("comment_count")
    private int commentCount;

    private int categoryId;
    private UserSummaryDTO user;

    @JsonManagedReference
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<CommentDTO> comments;

    @JsonManagedReference
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<PostMediaDTO> media;

    @JsonManagedReference
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<TagDTO> tags;

    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime updatedAt;

}
