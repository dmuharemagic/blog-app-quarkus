package edu.rit.croatia.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

@Data
public class PostMediaDTO {

    @JsonManagedReference
    private PostDTO post;

    @JsonManagedReference
    private MediaDTO media;

}
