package edu.rit.croatia.dto.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class RegisterRequestDTO {

    @Size(min = 5, max = 50, message = "Username must be between 5 and 50 characters.")
    @NotBlank(message = "Username must not be an empty field.")
    private String username;

    @Email(message = "Email must be a well-formed email address.")
    @NotBlank(message = "Email must not be an empty field.")
    private String email;

    @Size(min = 10, max = 255, message = "Password must be between 10 and 255 characters.")
    @NotBlank(message = "Password must not be an empty field.")
    private String password;

}