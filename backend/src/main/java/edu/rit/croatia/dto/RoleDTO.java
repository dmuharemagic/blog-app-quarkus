package edu.rit.croatia.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class RoleDTO {

    private String role;

    @JsonBackReference
    private Set<UserDTO> users = new HashSet<>();

}
