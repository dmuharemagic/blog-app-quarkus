package edu.rit.croatia.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonPropertyOrder({"id", "username", "email", "post_likes", "comment_likes"})
public class UserDTO {

    private long id;

    private String username;
    private String email;

    @JsonProperty("post_likes")
    private int postLikeCount;
    @JsonProperty("comment_likes")
    private int commentLikeCount;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonBackReference
    private Set<PostDTO> posts = new HashSet<>();

    @JsonManagedReference
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Set<CommentDTO> comments = new HashSet<>();

    //private List<MediaDTO> media = new ArrayList<>();

    @JsonManagedReference
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Set<RoleDTO> roles = new HashSet<>();

    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime updatedAt;

}
