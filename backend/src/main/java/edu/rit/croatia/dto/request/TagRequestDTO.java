package edu.rit.croatia.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TagRequestDTO {

    @NotBlank(message = "Tag slug must not be an empty field.")
    private String slug;

}
