package edu.rit.croatia.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginRequestDTO {

    @NotBlank(message = "Username must not be an empty field.")
    private String username;

    @NotBlank(message = "Password must not be an empty field.")
    private String password;
    
}