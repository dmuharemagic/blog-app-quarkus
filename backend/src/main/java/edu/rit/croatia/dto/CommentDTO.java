package edu.rit.croatia.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonPropertyOrder({"id", "body", "likes", "isDeleted"})
public class CommentDTO {

    private long id;
    private String body;

    private boolean isDeleted;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private long parentId;

    @JsonProperty("likes")
    private int commentLikes;

    @JsonManagedReference
    private Set<CommentDTO> comments = new HashSet<>();

    @JsonManagedReference
    private UserSummaryDTO user;

    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime updatedAt;

}
