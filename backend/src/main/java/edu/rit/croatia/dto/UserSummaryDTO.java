package edu.rit.croatia.dto;

import lombok.Data;

@Data
public class UserSummaryDTO {

    private long id;
    private String username;

}
