package edu.rit.croatia.dto;

import lombok.Data;

@Data
public class TagDTO {

    private Long id;
    private String slug;

}
