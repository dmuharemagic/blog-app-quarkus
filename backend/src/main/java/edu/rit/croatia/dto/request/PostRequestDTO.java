package edu.rit.croatia.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
public class PostRequestDTO {

    @Size(min = 10, max = 255, message = "Title must be between 10 and 255 characters.")
    @NotBlank(message = "Title must not be an empty field.")
    private String title;

    @Size(min = 10, message = "The post's content must contain a minimum of 10 characters.")
    @NotBlank(message = "Content must not be an empty field.")
    private String content;

    @Size(min = 5, max = 255, message = "Slug must be between 5 and 255 characters.")
    @NotBlank(message = "Slug must not be an empty field.")
    private String slug;

    @NotNull(message = "Category ID must not be empty.")
    private Long categoryId;

    @NotNull(message = "User ID must not be empty.")
    private Long userId;

    private Set<TagRequestDTO> tags;

}