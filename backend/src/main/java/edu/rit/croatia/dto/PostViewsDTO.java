package edu.rit.croatia.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class PostViewsDTO {

    private int count;
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm")
    private LocalDateTime lastViewed;

}
