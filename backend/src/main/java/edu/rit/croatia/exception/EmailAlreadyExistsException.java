package edu.rit.croatia.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class EmailAlreadyExistsException extends WebApplicationException {
    public EmailAlreadyExistsException() {
        super("A user with that email already exists.", Response.Status.BAD_REQUEST);
    }
}
