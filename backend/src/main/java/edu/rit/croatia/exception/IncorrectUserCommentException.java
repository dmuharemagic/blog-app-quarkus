package edu.rit.croatia.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class IncorrectUserCommentException extends WebApplicationException {
    public IncorrectUserCommentException() {
        super("The user specified is not owner of this particular comment.", Response.Status.BAD_REQUEST);
    }
}
