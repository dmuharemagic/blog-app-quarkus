package edu.rit.croatia.exception.response;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@JsonRootName(value = "error")
@AllArgsConstructor
@Getter
@Setter
public class ErrorResponse {

    private int status;
    private String description;
    private String message;
    
}