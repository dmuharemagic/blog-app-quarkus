package edu.rit.croatia.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class IncorrectLoginException extends WebApplicationException {
    public IncorrectLoginException() {
        super("Incorrect username or password.", Response.Status.BAD_REQUEST);
    }
}
