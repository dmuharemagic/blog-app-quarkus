package edu.rit.croatia.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class UsernameAlreadyExistsException extends WebApplicationException {
    public UsernameAlreadyExistsException() {
        super("A user with that username already exists.", Response.Status.BAD_REQUEST);
    }
}
