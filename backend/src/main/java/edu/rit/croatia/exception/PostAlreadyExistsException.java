package edu.rit.croatia.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class PostAlreadyExistsException extends WebApplicationException {
    public PostAlreadyExistsException() {
        super("A post with that slug already exists.", Response.Status.BAD_REQUEST);
    }
}
