package edu.rit.croatia.exception.response;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@JsonRootName(value = "error")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HibernateErrorResponse {

    private int status;
    private List<HibernateErrorDetails> errors;

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class HibernateErrorDetails {
        private String field;
        private String message;
    }

}