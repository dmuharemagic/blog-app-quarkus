package edu.rit.croatia.resource;

import edu.rit.croatia.dto.request.LoginRequestDTO;
import edu.rit.croatia.dto.request.RegisterRequestDTO;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import edu.rit.croatia.mapper.request.LoginRequestMapper;
import edu.rit.croatia.mapper.request.RegisterRequestMapper;
import edu.rit.croatia.provider.TokenProvider;
import edu.rit.croatia.service.AuthService;
import io.quarkus.security.Authenticated;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.annotation.security.PermitAll;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class AuthResource {

    @Context
    UriInfo uriInfo;

    private final AuthService service;

    private final JsonWebToken token;

    private final RegisterRequestMapper registerRequestMapper;
    private final LoginRequestMapper loginRequestMapper;

    @Inject
    public AuthResource(AuthService service, JsonWebToken token, RegisterRequestMapper registerRequestMapper, LoginRequestMapper loginRequestMapper) {
        this.service = service;
        this.token = token;
        this.registerRequestMapper = registerRequestMapper;
        this.loginRequestMapper = loginRequestMapper;
    }

    @POST
    @Path("/login")
    @PermitAll
    public Response login(@Valid LoginRequestDTO request) throws Exception {
        var user = service.authenticate(loginRequestMapper.toDomain(request, new CycleAvoidingMappingContext()));
        return Response.ok(TokenProvider.generateTokenString(user)).build();
    }

    @POST
    @Path("/register")
    @PermitAll
    public Response add(@Valid RegisterRequestDTO request) {
        service.register(registerRequestMapper.toDomain(request, new CycleAvoidingMappingContext()));
        return Response.status(201).build();
    }

    @GET
    @Path("/token/refresh")
    @Authenticated
    public Response refreshToken() throws Exception {
        return Response.ok(TokenProvider.refreshToken(token)).build();
    }

}