package edu.rit.croatia.resource;

import edu.rit.croatia.PageRequest;
import edu.rit.croatia.mapper.UserMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import edu.rit.croatia.service.UserService;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class UserResource {

    private final UserService service;

    private final UserMapper mapper;

    @Inject
    public UserResource(UserService service, UserMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GET
    @PermitAll
    public Response getAll(@BeanParam PageRequest request) {
        var posts = service.getAll(request);
        return Response.ok(mapper.toListResource(posts, new CycleAvoidingMappingContext())).build();
    }

    @GET
    @Path("{id}")
    @PermitAll
    public Response get(@PathParam("id") Long id) {
        var user = service.get(id);
        return Response.ok(mapper.toResource(user, new CycleAvoidingMappingContext())).build();
    }

    @GET
    @RolesAllowed("Admin")
    @Path("/admin")
    public Response getAdmin() {
        return Response.ok("Admin is here").build();
    }

    @GET
    @RolesAllowed("User")
    @Path("/me")
    public String me(@Context SecurityContext securityContext) {
        return securityContext.getUserPrincipal().getName();
    }

}