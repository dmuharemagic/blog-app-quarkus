package edu.rit.croatia.resource;

import edu.rit.croatia.dto.HasLikedDTO;
import edu.rit.croatia.dto.request.CommentRequestDTO;
import edu.rit.croatia.mapper.CommentMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import edu.rit.croatia.mapper.request.CommentRequestMapper;
import edu.rit.croatia.service.CommentService;
import io.quarkus.security.Authenticated;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/comments")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentResource {

    private final CommentService service;

    private final CommentMapper commentMapper;
    private final CommentRequestMapper commentRequestMapper;

    @Inject
    public CommentResource(CommentService service, CommentMapper commentMapper, CommentRequestMapper commentRequestMapper) {
        this.service = service;
        this.commentMapper = commentMapper;
        this.commentRequestMapper = commentRequestMapper;
    }

    @GET
    @PermitAll
    public Response getList() {
        var comments = service.getAll();
        return Response.ok(commentMapper.toListResource(comments, new CycleAvoidingMappingContext())).build();
    }

    @GET
    @PermitAll
    @Path("{id}")
    public Response getOne(@PathParam("id") long id) {
        var comment = service.get(id);
        return Response.ok(commentMapper.toResource(comment, new CycleAvoidingMappingContext())).build();
    }

    @POST
    @Authenticated
    public Response add(@Valid CommentRequestDTO request) {
        var comment = service.add(commentRequestMapper.toDomain(request, new CycleAvoidingMappingContext()));
        return Response.status(Response.Status.CREATED).entity(commentMapper.toResource(comment, new CycleAvoidingMappingContext())).build();
    }

    @DELETE
    @Authenticated
    @Path("{id}")
    public Response delete(@PathParam("id") long id, @Context SecurityContext securityContext) {
        service.delete(id, securityContext.getUserPrincipal().getName());
        return Response.noContent().build();
    }

    @GET
    @Path("{id}/vote")
    @Authenticated
    public Response hasVoted(@PathParam("id") long id, @Context SecurityContext securityContext) {
        boolean liked = service.getVote(id, securityContext.getUserPrincipal().getName());
        return Response.ok(new HasLikedDTO(liked), MediaType.APPLICATION_JSON_TYPE).build();
    }

    @POST
    @Path("{id}/vote")
    @Authenticated
    public Response castVote(@PathParam("id") long id, @Context SecurityContext securityContext) {
        service.castVote(id, securityContext.getUserPrincipal().getName());
        return Response.status(201).build();
    }

}


