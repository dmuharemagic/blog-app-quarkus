package edu.rit.croatia.resource;

import edu.rit.croatia.mapper.TagMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import edu.rit.croatia.service.TagService;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/tags")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TagResource {

    private final TagService service;

    private final TagMapper mapper;

    @Inject
    public TagResource(TagService service, TagMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GET
    @PermitAll
    public Response getList() {
        var tags = service.getAll();
        return Response.ok(mapper.toListResource(tags, new CycleAvoidingMappingContext())).build();
    }

    @GET
    @PermitAll
    @Path("{slug}")
    public Response getOne(@PathParam("slug") String slug) {
        var tag = service.get(slug);
        return Response.ok(mapper.toResource(tag, new CycleAvoidingMappingContext())).build();
    }
    
    @DELETE
    @RolesAllowed("Admin")
    @Path("{id}")
    public Response delete(@PathParam("id") long id) {
        service.delete(id);
        return Response.noContent().build();
    }

}


