package edu.rit.croatia.resource;

import edu.rit.croatia.mapper.CategoryMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import edu.rit.croatia.service.CategoryService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoryResource {

    private final CategoryService service;

    private final CategoryMapper mapper;

    @Inject
    public CategoryResource(CategoryService service, CategoryMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GET
    public Response getList() {
        var categories = service.getAll();
        return Response.ok(mapper.toListResource(categories, new CycleAvoidingMappingContext())).build();
    }

    @GET
    @Path("{id}")
    public Response getOne(@PathParam("id") long id) {
        var category = service.get(id);
        return Response.ok(mapper.toResource(category, new CycleAvoidingMappingContext())).build();
    }

    @DELETE
    @RolesAllowed("Admin")
    @Path("{id}")
    public Response delete(@PathParam("id") long id) {
        service.delete(id);
        return Response.noContent().build();
    }

}


