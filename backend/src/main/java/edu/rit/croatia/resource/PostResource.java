package edu.rit.croatia.resource;

import edu.rit.croatia.PageRequest;
import edu.rit.croatia.dto.HasLikedDTO;
import edu.rit.croatia.dto.PostViewsDTO;
import edu.rit.croatia.dto.request.PostRequestDTO;
import edu.rit.croatia.mapper.PostDetailMapper;
import edu.rit.croatia.mapper.PostMapper;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import edu.rit.croatia.service.PostService;
import io.quarkus.security.Authenticated;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/posts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostResource {

    private final PostService service;

    private final PostMapper postMapper;
    private final PostDetailMapper postDetailMapper;

    public PostResource(PostService service, PostMapper postMapper, PostDetailMapper postDetailMapper) {
        this.service = service;
        this.postMapper = postMapper;
        this.postDetailMapper = postDetailMapper;
    }

    @GET
    @PermitAll
    public Response getAll(@BeanParam PageRequest request) {
        var posts = service.getAll(request);
        return Response.ok(postMapper.toListResource(posts, new CycleAvoidingMappingContext())).build();
    }

    @GET
    @PermitAll
    @Path("{id}/{slug}")
    public Response get(@PathParam("id") long id, @PathParam("slug") String slug) {
        var post = service.get(id, slug);
        return Response.ok(postDetailMapper.toResource(post, new CycleAvoidingMappingContext())).build();
    }

    @POST
    @Authenticated
    public Response add(@Valid PostRequestDTO request) {
        var post = service.add(request);
        return Response.status(Response.Status.CREATED).entity(postMapper.toResource(post, new CycleAvoidingMappingContext())).build();
    }

    @GET
    @PermitAll
    @Path("{id}/views")
    public Response getViews(@PathParam("id") long id) {
        var views = service.getViews(id);
        return Response.ok(new PostViewsDTO(views.getCount(), views.getLastViewed()), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @PermitAll
    @Path("{id}/views")
    public Response updateViewCount(@PathParam("id") long id) {
        var views = service.updateViewCount(id);
        return Response.ok(new PostViewsDTO(views.getCount(), views.getLastViewed()), MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("{id}/{slug}/vote")
    @Authenticated
    public Response getVote(@PathParam("id") long id, @PathParam("slug") String slug, @Context SecurityContext securityContext) {
        boolean liked = service.hasVoted(id, slug, securityContext.getUserPrincipal().getName());
        return Response.ok(new HasLikedDTO(liked), MediaType.APPLICATION_JSON_TYPE).build();
    }

    @POST
    @Path("{id}/{slug}/vote")
    @Authenticated
    public Response castVote(@PathParam("id") long id, @PathParam("slug") String slug, @Context SecurityContext securityContext) {
        service.castVote(id, slug, securityContext.getUserPrincipal().getName());
        return Response.status(201).build();
    }

}