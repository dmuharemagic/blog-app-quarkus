package edu.rit.croatia;

import lombok.Getter;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

@Getter
public class PageRequest {

    @QueryParam("page")
    @DefaultValue("0")
    private int page;

    @QueryParam("size")
    @DefaultValue("10")
    private int size;

}
