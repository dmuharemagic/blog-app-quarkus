package edu.rit.croatia.mapper;

import edu.rit.croatia.domain.Comment;
import edu.rit.croatia.domain.Media;
import edu.rit.croatia.domain.Post;
import edu.rit.croatia.domain.PostMedia;
import edu.rit.croatia.domain.Role;
import edu.rit.croatia.domain.Tag;
import edu.rit.croatia.domain.User;
import edu.rit.croatia.dto.CommentDTO;
import edu.rit.croatia.dto.MediaDTO;
import edu.rit.croatia.dto.PostDTO;
import edu.rit.croatia.dto.PostMediaDTO;
import edu.rit.croatia.dto.RoleDTO;
import edu.rit.croatia.dto.TagDTO;
import edu.rit.croatia.dto.UserDTO;
import edu.rit.croatia.dto.UserSummaryDTO;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import javax.enterprise.context.ApplicationScoped;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-21T22:20:15+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 13.0.2 (AdoptOpenJDK)"
)
@ApplicationScoped
public class PostMapperImpl implements PostMapper {

    @Override
    public List<Post> toDomain(List<PostDTO> dto, CycleAvoidingMappingContext context) {
        List<Post> target = context.getMappedInstance( dto, List.class );
        if ( target != null ) {
            return target;
        }

        if ( dto == null ) {
            return null;
        }

        List<Post> list = new ArrayList<Post>( dto.size() );
        context.storeMappedInstance( dto, list );

        for ( PostDTO postDTO : dto ) {
            list.add( toDomain( postDTO, context ) );
        }

        return list;
    }

    @Override
    public Post toDomain(PostDTO dto, CycleAvoidingMappingContext context) {
        Post target = context.getMappedInstance( dto, Post.class );
        if ( target != null ) {
            return target;
        }

        if ( dto == null ) {
            return null;
        }

        Post post = new Post();

        context.storeMappedInstance( dto, post );

        post.setId( dto.getId() );
        post.setTitle( dto.getTitle() );
        post.setContent( dto.getContent() );
        post.setSlug( dto.getSlug() );
        post.setCategoryId( (long) dto.getCategoryId() );
        post.setCreatedAt( dto.getCreatedAt() );
        post.setUpdatedAt( dto.getUpdatedAt() );
        post.setComments( commentDTOSetToCommentSet( dto.getComments(), context ) );
        post.setCommentCount( dto.getCommentCount() );
        post.setLikeCount( dto.getLikeCount() );
        post.setUser( userSummaryDTOToUser( dto.getUser(), context ) );
        post.setMedia( postMediaDTOListToPostMediaList( dto.getMedia(), context ) );
        post.setTags( tagDTOSetToTagSet( dto.getTags(), context ) );
        post.setViewCount( dto.getViewCount() );

        return post;
    }

    @Override
    public List<PostDTO> toListResource(List<Post> entities, CycleAvoidingMappingContext context) {
        List<PostDTO> target = context.getMappedInstance( entities, List.class );
        if ( target != null ) {
            return target;
        }

        if ( entities == null ) {
            return null;
        }

        List<PostDTO> list = new ArrayList<PostDTO>( entities.size() );
        context.storeMappedInstance( entities, list );

        for ( Post post : entities ) {
            list.add( toResource( post, context ) );
        }

        return list;
    }

    @Override
    public PostDTO toResource(Post entity, CycleAvoidingMappingContext context) {
        PostDTO target = context.getMappedInstance( entity, PostDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( entity == null ) {
            return null;
        }

        PostDTO postDTO = new PostDTO();

        context.storeMappedInstance( entity, postDTO );

        if ( entity.getId() != null ) {
            postDTO.setId( entity.getId() );
        }
        postDTO.setTitle( entity.getTitle() );
        postDTO.setContent( entity.getContent() );
        postDTO.setSlug( entity.getSlug() );
        postDTO.setViewCount( entity.getViewCount() );
        postDTO.setLikeCount( entity.getLikeCount() );
        postDTO.setCommentCount( entity.getCommentCount() );
        if ( entity.getCategoryId() != null ) {
            postDTO.setCategoryId( entity.getCategoryId().intValue() );
        }
        postDTO.setUser( userToUserSummaryDTO( entity.getUser(), context ) );
        postDTO.setTags( tagSetToTagDTOSet( entity.getTags(), context ) );
        postDTO.setCreatedAt( entity.getCreatedAt() );
        postDTO.setUpdatedAt( entity.getUpdatedAt() );

        return postDTO;
    }

    protected Set<Comment> commentDTOSetToCommentSet(Set<CommentDTO> set, CycleAvoidingMappingContext context) {
        Set<Comment> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<Comment> set1 = new HashSet<Comment>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( CommentDTO commentDTO : set ) {
            set1.add( commentDTOToComment( commentDTO, context ) );
        }

        return set1;
    }

    protected User userSummaryDTOToUser(UserSummaryDTO userSummaryDTO, CycleAvoidingMappingContext context) {
        User target = context.getMappedInstance( userSummaryDTO, User.class );
        if ( target != null ) {
            return target;
        }

        if ( userSummaryDTO == null ) {
            return null;
        }

        User user = new User();

        context.storeMappedInstance( userSummaryDTO, user );

        user.setId( userSummaryDTO.getId() );
        user.setUsername( userSummaryDTO.getUsername() );

        return user;
    }

    protected Comment commentDTOToComment(CommentDTO commentDTO, CycleAvoidingMappingContext context) {
        Comment target = context.getMappedInstance( commentDTO, Comment.class );
        if ( target != null ) {
            return target;
        }

        if ( commentDTO == null ) {
            return null;
        }

        Comment comment = new Comment();

        context.storeMappedInstance( commentDTO, comment );

        comment.setId( commentDTO.getId() );
        comment.setParentId( commentDTO.getParentId() );
        comment.setBody( commentDTO.getBody() );
        comment.setDeleted( commentDTO.isDeleted() );
        comment.setCreatedAt( commentDTO.getCreatedAt() );
        comment.setUpdatedAt( commentDTO.getUpdatedAt() );
        comment.setComments( commentDTOSetToCommentSet( commentDTO.getComments(), context ) );
        comment.setCommentLikes( commentDTO.getCommentLikes() );
        comment.setUser( userSummaryDTOToUser( commentDTO.getUser(), context ) );

        return comment;
    }

    protected Set<Post> postDTOSetToPostSet(Set<PostDTO> set, CycleAvoidingMappingContext context) {
        Set<Post> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<Post> set1 = new HashSet<Post>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( PostDTO postDTO : set ) {
            set1.add( toDomain( postDTO, context ) );
        }

        return set1;
    }

    protected Set<User> userDTOSetToUserSet(Set<UserDTO> set, CycleAvoidingMappingContext context) {
        Set<User> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<User> set1 = new HashSet<User>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( UserDTO userDTO : set ) {
            set1.add( userDTOToUser( userDTO, context ) );
        }

        return set1;
    }

    protected Role roleDTOToRole(RoleDTO roleDTO, CycleAvoidingMappingContext context) {
        Role target = context.getMappedInstance( roleDTO, Role.class );
        if ( target != null ) {
            return target;
        }

        if ( roleDTO == null ) {
            return null;
        }

        Role role = new Role();

        context.storeMappedInstance( roleDTO, role );

        role.setRole( roleDTO.getRole() );
        role.setUsers( userDTOSetToUserSet( roleDTO.getUsers(), context ) );

        return role;
    }

    protected Set<Role> roleDTOSetToRoleSet(Set<RoleDTO> set, CycleAvoidingMappingContext context) {
        Set<Role> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<Role> set1 = new HashSet<Role>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( RoleDTO roleDTO : set ) {
            set1.add( roleDTOToRole( roleDTO, context ) );
        }

        return set1;
    }

    protected User userDTOToUser(UserDTO userDTO, CycleAvoidingMappingContext context) {
        User target = context.getMappedInstance( userDTO, User.class );
        if ( target != null ) {
            return target;
        }

        if ( userDTO == null ) {
            return null;
        }

        User user = new User();

        context.storeMappedInstance( userDTO, user );

        user.setId( userDTO.getId() );
        user.setUsername( userDTO.getUsername() );
        user.setEmail( userDTO.getEmail() );
        user.setCreatedAt( userDTO.getCreatedAt() );
        user.setUpdatedAt( userDTO.getUpdatedAt() );
        user.setComments( commentDTOSetToCommentSet( userDTO.getComments(), context ) );
        user.setCommentLikeCount( userDTO.getCommentLikeCount() );
        user.setPosts( postDTOSetToPostSet( userDTO.getPosts(), context ) );
        user.setPostLikeCount( userDTO.getPostLikeCount() );
        user.setRoles( roleDTOSetToRoleSet( userDTO.getRoles(), context ) );

        return user;
    }

    protected List<PostMedia> postMediaDTOListToPostMediaList(List<PostMediaDTO> list, CycleAvoidingMappingContext context) {
        List<PostMedia> target = context.getMappedInstance( list, List.class );
        if ( target != null ) {
            return target;
        }

        if ( list == null ) {
            return null;
        }

        List<PostMedia> list1 = new ArrayList<PostMedia>( list.size() );
        context.storeMappedInstance( list, list1 );

        for ( PostMediaDTO postMediaDTO : list ) {
            list1.add( postMediaDTOToPostMedia( postMediaDTO, context ) );
        }

        return list1;
    }

    protected Media mediaDTOToMedia(MediaDTO mediaDTO, CycleAvoidingMappingContext context) {
        Media target = context.getMappedInstance( mediaDTO, Media.class );
        if ( target != null ) {
            return target;
        }

        if ( mediaDTO == null ) {
            return null;
        }

        Media media = new Media();

        context.storeMappedInstance( mediaDTO, media );

        media.setUrl( mediaDTO.getUrl() );
        media.setCreatedAt( mediaDTO.getCreatedAt() );
        media.setUpdatedAt( mediaDTO.getUpdatedAt() );
        media.setUser( userDTOToUser( mediaDTO.getUser(), context ) );
        media.setPosts( postMediaDTOListToPostMediaList( mediaDTO.getPosts(), context ) );

        return media;
    }

    protected PostMedia postMediaDTOToPostMedia(PostMediaDTO postMediaDTO, CycleAvoidingMappingContext context) {
        PostMedia target = context.getMappedInstance( postMediaDTO, PostMedia.class );
        if ( target != null ) {
            return target;
        }

        if ( postMediaDTO == null ) {
            return null;
        }

        PostMedia postMedia = new PostMedia();

        context.storeMappedInstance( postMediaDTO, postMedia );

        postMedia.setPost( toDomain( postMediaDTO.getPost(), context ) );
        postMedia.setMedia( mediaDTOToMedia( postMediaDTO.getMedia(), context ) );

        return postMedia;
    }

    protected Tag tagDTOToTag(TagDTO tagDTO, CycleAvoidingMappingContext context) {
        Tag target = context.getMappedInstance( tagDTO, Tag.class );
        if ( target != null ) {
            return target;
        }

        if ( tagDTO == null ) {
            return null;
        }

        Tag tag = new Tag();

        context.storeMappedInstance( tagDTO, tag );

        tag.setId( tagDTO.getId() );
        tag.setSlug( tagDTO.getSlug() );

        return tag;
    }

    protected Set<Tag> tagDTOSetToTagSet(Set<TagDTO> set, CycleAvoidingMappingContext context) {
        Set<Tag> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<Tag> set1 = new HashSet<Tag>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( TagDTO tagDTO : set ) {
            set1.add( tagDTOToTag( tagDTO, context ) );
        }

        return set1;
    }

    protected UserSummaryDTO userToUserSummaryDTO(User user, CycleAvoidingMappingContext context) {
        UserSummaryDTO target = context.getMappedInstance( user, UserSummaryDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( user == null ) {
            return null;
        }

        UserSummaryDTO userSummaryDTO = new UserSummaryDTO();

        context.storeMappedInstance( user, userSummaryDTO );

        if ( user.getId() != null ) {
            userSummaryDTO.setId( user.getId() );
        }
        userSummaryDTO.setUsername( user.getUsername() );

        return userSummaryDTO;
    }

    protected TagDTO tagToTagDTO(Tag tag, CycleAvoidingMappingContext context) {
        TagDTO target = context.getMappedInstance( tag, TagDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( tag == null ) {
            return null;
        }

        TagDTO tagDTO = new TagDTO();

        context.storeMappedInstance( tag, tagDTO );

        tagDTO.setId( tag.getId() );
        tagDTO.setSlug( tag.getSlug() );

        return tagDTO;
    }

    protected Set<TagDTO> tagSetToTagDTOSet(Set<Tag> set, CycleAvoidingMappingContext context) {
        Set<TagDTO> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<TagDTO> set1 = new HashSet<TagDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( Tag tag : set ) {
            set1.add( tagToTagDTO( tag, context ) );
        }

        return set1;
    }
}
