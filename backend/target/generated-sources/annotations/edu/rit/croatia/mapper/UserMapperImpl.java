package edu.rit.croatia.mapper;

import edu.rit.croatia.domain.Comment;
import edu.rit.croatia.domain.Media;
import edu.rit.croatia.domain.Post;
import edu.rit.croatia.domain.PostMedia;
import edu.rit.croatia.domain.Role;
import edu.rit.croatia.domain.Tag;
import edu.rit.croatia.domain.User;
import edu.rit.croatia.dto.CommentDTO;
import edu.rit.croatia.dto.MediaDTO;
import edu.rit.croatia.dto.PostDTO;
import edu.rit.croatia.dto.PostMediaDTO;
import edu.rit.croatia.dto.RoleDTO;
import edu.rit.croatia.dto.TagDTO;
import edu.rit.croatia.dto.UserDTO;
import edu.rit.croatia.dto.UserSummaryDTO;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import javax.enterprise.context.ApplicationScoped;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-21T22:20:14+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 13.0.2 (AdoptOpenJDK)"
)
@ApplicationScoped
public class UserMapperImpl implements UserMapper {

    @Override
    public List<User> toDomain(List<UserDTO> dto, CycleAvoidingMappingContext context) {
        List<User> target = context.getMappedInstance( dto, List.class );
        if ( target != null ) {
            return target;
        }

        if ( dto == null ) {
            return null;
        }

        List<User> list = new ArrayList<User>( dto.size() );
        context.storeMappedInstance( dto, list );

        for ( UserDTO userDTO : dto ) {
            list.add( toDomain( userDTO, context ) );
        }

        return list;
    }

    @Override
    public User toDomain(UserDTO dto, CycleAvoidingMappingContext context) {
        User target = context.getMappedInstance( dto, User.class );
        if ( target != null ) {
            return target;
        }

        if ( dto == null ) {
            return null;
        }

        User user = new User();

        context.storeMappedInstance( dto, user );

        user.setId( dto.getId() );
        user.setUsername( dto.getUsername() );
        user.setEmail( dto.getEmail() );
        user.setCreatedAt( dto.getCreatedAt() );
        user.setUpdatedAt( dto.getUpdatedAt() );
        user.setComments( commentDTOSetToCommentSet( dto.getComments(), context ) );
        user.setCommentLikeCount( dto.getCommentLikeCount() );
        user.setPosts( postDTOSetToPostSet( dto.getPosts(), context ) );
        user.setPostLikeCount( dto.getPostLikeCount() );
        user.setRoles( roleDTOSetToRoleSet( dto.getRoles(), context ) );

        return user;
    }

    @Override
    public List<UserDTO> toListResource(List<User> entities, CycleAvoidingMappingContext context) {
        List<UserDTO> target = context.getMappedInstance( entities, List.class );
        if ( target != null ) {
            return target;
        }

        if ( entities == null ) {
            return null;
        }

        List<UserDTO> list = new ArrayList<UserDTO>( entities.size() );
        context.storeMappedInstance( entities, list );

        for ( User user : entities ) {
            list.add( toResource( user, context ) );
        }

        return list;
    }

    @Override
    public UserDTO toResource(User entity, CycleAvoidingMappingContext context) {
        UserDTO target = context.getMappedInstance( entity, UserDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( entity == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        context.storeMappedInstance( entity, userDTO );

        if ( entity.getId() != null ) {
            userDTO.setId( entity.getId() );
        }
        userDTO.setUsername( entity.getUsername() );
        userDTO.setEmail( entity.getEmail() );
        userDTO.setPostLikeCount( entity.getPostLikeCount() );
        userDTO.setCommentLikeCount( entity.getCommentLikeCount() );
        userDTO.setRoles( roleSetToRoleDTOSet( entity.getRoles(), context ) );
        userDTO.setCreatedAt( entity.getCreatedAt() );
        userDTO.setUpdatedAt( entity.getUpdatedAt() );

        return userDTO;
    }

    protected Set<Comment> commentDTOSetToCommentSet(Set<CommentDTO> set, CycleAvoidingMappingContext context) {
        Set<Comment> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<Comment> set1 = new HashSet<Comment>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( CommentDTO commentDTO : set ) {
            set1.add( commentDTOToComment( commentDTO, context ) );
        }

        return set1;
    }

    protected User userSummaryDTOToUser(UserSummaryDTO userSummaryDTO, CycleAvoidingMappingContext context) {
        User target = context.getMappedInstance( userSummaryDTO, User.class );
        if ( target != null ) {
            return target;
        }

        if ( userSummaryDTO == null ) {
            return null;
        }

        User user = new User();

        context.storeMappedInstance( userSummaryDTO, user );

        user.setId( userSummaryDTO.getId() );
        user.setUsername( userSummaryDTO.getUsername() );

        return user;
    }

    protected Comment commentDTOToComment(CommentDTO commentDTO, CycleAvoidingMappingContext context) {
        Comment target = context.getMappedInstance( commentDTO, Comment.class );
        if ( target != null ) {
            return target;
        }

        if ( commentDTO == null ) {
            return null;
        }

        Comment comment = new Comment();

        context.storeMappedInstance( commentDTO, comment );

        comment.setId( commentDTO.getId() );
        comment.setParentId( commentDTO.getParentId() );
        comment.setBody( commentDTO.getBody() );
        comment.setDeleted( commentDTO.isDeleted() );
        comment.setCreatedAt( commentDTO.getCreatedAt() );
        comment.setUpdatedAt( commentDTO.getUpdatedAt() );
        comment.setComments( commentDTOSetToCommentSet( commentDTO.getComments(), context ) );
        comment.setCommentLikes( commentDTO.getCommentLikes() );
        comment.setUser( userSummaryDTOToUser( commentDTO.getUser(), context ) );

        return comment;
    }

    protected List<PostMedia> postMediaDTOListToPostMediaList(List<PostMediaDTO> list, CycleAvoidingMappingContext context) {
        List<PostMedia> target = context.getMappedInstance( list, List.class );
        if ( target != null ) {
            return target;
        }

        if ( list == null ) {
            return null;
        }

        List<PostMedia> list1 = new ArrayList<PostMedia>( list.size() );
        context.storeMappedInstance( list, list1 );

        for ( PostMediaDTO postMediaDTO : list ) {
            list1.add( postMediaDTOToPostMedia( postMediaDTO, context ) );
        }

        return list1;
    }

    protected Media mediaDTOToMedia(MediaDTO mediaDTO, CycleAvoidingMappingContext context) {
        Media target = context.getMappedInstance( mediaDTO, Media.class );
        if ( target != null ) {
            return target;
        }

        if ( mediaDTO == null ) {
            return null;
        }

        Media media = new Media();

        context.storeMappedInstance( mediaDTO, media );

        media.setUrl( mediaDTO.getUrl() );
        media.setCreatedAt( mediaDTO.getCreatedAt() );
        media.setUpdatedAt( mediaDTO.getUpdatedAt() );
        media.setUser( toDomain( mediaDTO.getUser(), context ) );
        media.setPosts( postMediaDTOListToPostMediaList( mediaDTO.getPosts(), context ) );

        return media;
    }

    protected PostMedia postMediaDTOToPostMedia(PostMediaDTO postMediaDTO, CycleAvoidingMappingContext context) {
        PostMedia target = context.getMappedInstance( postMediaDTO, PostMedia.class );
        if ( target != null ) {
            return target;
        }

        if ( postMediaDTO == null ) {
            return null;
        }

        PostMedia postMedia = new PostMedia();

        context.storeMappedInstance( postMediaDTO, postMedia );

        postMedia.setPost( postDTOToPost( postMediaDTO.getPost(), context ) );
        postMedia.setMedia( mediaDTOToMedia( postMediaDTO.getMedia(), context ) );

        return postMedia;
    }

    protected Tag tagDTOToTag(TagDTO tagDTO, CycleAvoidingMappingContext context) {
        Tag target = context.getMappedInstance( tagDTO, Tag.class );
        if ( target != null ) {
            return target;
        }

        if ( tagDTO == null ) {
            return null;
        }

        Tag tag = new Tag();

        context.storeMappedInstance( tagDTO, tag );

        tag.setId( tagDTO.getId() );
        tag.setSlug( tagDTO.getSlug() );

        return tag;
    }

    protected Set<Tag> tagDTOSetToTagSet(Set<TagDTO> set, CycleAvoidingMappingContext context) {
        Set<Tag> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<Tag> set1 = new HashSet<Tag>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( TagDTO tagDTO : set ) {
            set1.add( tagDTOToTag( tagDTO, context ) );
        }

        return set1;
    }

    protected Post postDTOToPost(PostDTO postDTO, CycleAvoidingMappingContext context) {
        Post target = context.getMappedInstance( postDTO, Post.class );
        if ( target != null ) {
            return target;
        }

        if ( postDTO == null ) {
            return null;
        }

        Post post = new Post();

        context.storeMappedInstance( postDTO, post );

        post.setId( postDTO.getId() );
        post.setTitle( postDTO.getTitle() );
        post.setContent( postDTO.getContent() );
        post.setSlug( postDTO.getSlug() );
        post.setCategoryId( (long) postDTO.getCategoryId() );
        post.setCreatedAt( postDTO.getCreatedAt() );
        post.setUpdatedAt( postDTO.getUpdatedAt() );
        post.setComments( commentDTOSetToCommentSet( postDTO.getComments(), context ) );
        post.setCommentCount( postDTO.getCommentCount() );
        post.setLikeCount( postDTO.getLikeCount() );
        post.setUser( userSummaryDTOToUser( postDTO.getUser(), context ) );
        post.setMedia( postMediaDTOListToPostMediaList( postDTO.getMedia(), context ) );
        post.setTags( tagDTOSetToTagSet( postDTO.getTags(), context ) );
        post.setViewCount( postDTO.getViewCount() );

        return post;
    }

    protected Set<Post> postDTOSetToPostSet(Set<PostDTO> set, CycleAvoidingMappingContext context) {
        Set<Post> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<Post> set1 = new HashSet<Post>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( PostDTO postDTO : set ) {
            set1.add( postDTOToPost( postDTO, context ) );
        }

        return set1;
    }

    protected Set<User> userDTOSetToUserSet(Set<UserDTO> set, CycleAvoidingMappingContext context) {
        Set<User> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<User> set1 = new HashSet<User>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( UserDTO userDTO : set ) {
            set1.add( toDomain( userDTO, context ) );
        }

        return set1;
    }

    protected Role roleDTOToRole(RoleDTO roleDTO, CycleAvoidingMappingContext context) {
        Role target = context.getMappedInstance( roleDTO, Role.class );
        if ( target != null ) {
            return target;
        }

        if ( roleDTO == null ) {
            return null;
        }

        Role role = new Role();

        context.storeMappedInstance( roleDTO, role );

        role.setRole( roleDTO.getRole() );
        role.setUsers( userDTOSetToUserSet( roleDTO.getUsers(), context ) );

        return role;
    }

    protected Set<Role> roleDTOSetToRoleSet(Set<RoleDTO> set, CycleAvoidingMappingContext context) {
        Set<Role> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<Role> set1 = new HashSet<Role>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( RoleDTO roleDTO : set ) {
            set1.add( roleDTOToRole( roleDTO, context ) );
        }

        return set1;
    }

    protected UserSummaryDTO userToUserSummaryDTO(User user, CycleAvoidingMappingContext context) {
        UserSummaryDTO target = context.getMappedInstance( user, UserSummaryDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( user == null ) {
            return null;
        }

        UserSummaryDTO userSummaryDTO = new UserSummaryDTO();

        context.storeMappedInstance( user, userSummaryDTO );

        if ( user.getId() != null ) {
            userSummaryDTO.setId( user.getId() );
        }
        userSummaryDTO.setUsername( user.getUsername() );

        return userSummaryDTO;
    }

    protected Set<CommentDTO> commentSetToCommentDTOSet(Set<Comment> set, CycleAvoidingMappingContext context) {
        Set<CommentDTO> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<CommentDTO> set1 = new HashSet<CommentDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( Comment comment : set ) {
            set1.add( commentToCommentDTO( comment, context ) );
        }

        return set1;
    }

    protected CommentDTO commentToCommentDTO(Comment comment, CycleAvoidingMappingContext context) {
        CommentDTO target = context.getMappedInstance( comment, CommentDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( comment == null ) {
            return null;
        }

        CommentDTO commentDTO = new CommentDTO();

        context.storeMappedInstance( comment, commentDTO );

        if ( comment.getId() != null ) {
            commentDTO.setId( comment.getId() );
        }
        commentDTO.setBody( comment.getBody() );
        commentDTO.setDeleted( comment.isDeleted() );
        if ( comment.getParentId() != null ) {
            commentDTO.setParentId( comment.getParentId() );
        }
        commentDTO.setCommentLikes( comment.getCommentLikes() );
        commentDTO.setComments( commentSetToCommentDTOSet( comment.getComments(), context ) );
        commentDTO.setUser( userToUserSummaryDTO( comment.getUser(), context ) );
        commentDTO.setCreatedAt( comment.getCreatedAt() );
        commentDTO.setUpdatedAt( comment.getUpdatedAt() );

        return commentDTO;
    }

    protected List<PostMediaDTO> postMediaListToPostMediaDTOList(List<PostMedia> list, CycleAvoidingMappingContext context) {
        List<PostMediaDTO> target = context.getMappedInstance( list, List.class );
        if ( target != null ) {
            return target;
        }

        if ( list == null ) {
            return null;
        }

        List<PostMediaDTO> list1 = new ArrayList<PostMediaDTO>( list.size() );
        context.storeMappedInstance( list, list1 );

        for ( PostMedia postMedia : list ) {
            list1.add( postMediaToPostMediaDTO( postMedia, context ) );
        }

        return list1;
    }

    protected MediaDTO mediaToMediaDTO(Media media, CycleAvoidingMappingContext context) {
        MediaDTO target = context.getMappedInstance( media, MediaDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( media == null ) {
            return null;
        }

        MediaDTO mediaDTO = new MediaDTO();

        context.storeMappedInstance( media, mediaDTO );

        mediaDTO.setUrl( media.getUrl() );
        mediaDTO.setCreatedAt( media.getCreatedAt() );
        mediaDTO.setUpdatedAt( media.getUpdatedAt() );
        mediaDTO.setUser( userToUserDTO( media.getUser(), context ) );
        mediaDTO.setPosts( postMediaListToPostMediaDTOList( media.getPosts(), context ) );

        return mediaDTO;
    }

    protected PostMediaDTO postMediaToPostMediaDTO(PostMedia postMedia, CycleAvoidingMappingContext context) {
        PostMediaDTO target = context.getMappedInstance( postMedia, PostMediaDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( postMedia == null ) {
            return null;
        }

        PostMediaDTO postMediaDTO = new PostMediaDTO();

        context.storeMappedInstance( postMedia, postMediaDTO );

        postMediaDTO.setPost( postToPostDTO( postMedia.getPost(), context ) );
        postMediaDTO.setMedia( mediaToMediaDTO( postMedia.getMedia(), context ) );

        return postMediaDTO;
    }

    protected TagDTO tagToTagDTO(Tag tag, CycleAvoidingMappingContext context) {
        TagDTO target = context.getMappedInstance( tag, TagDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( tag == null ) {
            return null;
        }

        TagDTO tagDTO = new TagDTO();

        context.storeMappedInstance( tag, tagDTO );

        tagDTO.setId( tag.getId() );
        tagDTO.setSlug( tag.getSlug() );

        return tagDTO;
    }

    protected Set<TagDTO> tagSetToTagDTOSet(Set<Tag> set, CycleAvoidingMappingContext context) {
        Set<TagDTO> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<TagDTO> set1 = new HashSet<TagDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( Tag tag : set ) {
            set1.add( tagToTagDTO( tag, context ) );
        }

        return set1;
    }

    protected PostDTO postToPostDTO(Post post, CycleAvoidingMappingContext context) {
        PostDTO target = context.getMappedInstance( post, PostDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( post == null ) {
            return null;
        }

        PostDTO postDTO = new PostDTO();

        context.storeMappedInstance( post, postDTO );

        if ( post.getId() != null ) {
            postDTO.setId( post.getId() );
        }
        postDTO.setTitle( post.getTitle() );
        postDTO.setContent( post.getContent() );
        postDTO.setSlug( post.getSlug() );
        postDTO.setViewCount( post.getViewCount() );
        postDTO.setLikeCount( post.getLikeCount() );
        postDTO.setCommentCount( post.getCommentCount() );
        if ( post.getCategoryId() != null ) {
            postDTO.setCategoryId( post.getCategoryId().intValue() );
        }
        postDTO.setUser( userToUserSummaryDTO( post.getUser(), context ) );
        postDTO.setComments( commentSetToCommentDTOSet( post.getComments(), context ) );
        postDTO.setMedia( postMediaListToPostMediaDTOList( post.getMedia(), context ) );
        postDTO.setTags( tagSetToTagDTOSet( post.getTags(), context ) );
        postDTO.setCreatedAt( post.getCreatedAt() );
        postDTO.setUpdatedAt( post.getUpdatedAt() );

        return postDTO;
    }

    protected Set<PostDTO> postSetToPostDTOSet(Set<Post> set, CycleAvoidingMappingContext context) {
        Set<PostDTO> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<PostDTO> set1 = new HashSet<PostDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( Post post : set ) {
            set1.add( postToPostDTO( post, context ) );
        }

        return set1;
    }

    protected Set<RoleDTO> roleSetToRoleDTOSet(Set<Role> set, CycleAvoidingMappingContext context) {
        Set<RoleDTO> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<RoleDTO> set1 = new HashSet<RoleDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( Role role : set ) {
            set1.add( roleToRoleDTO( role, context ) );
        }

        return set1;
    }

    protected UserDTO userToUserDTO(User user, CycleAvoidingMappingContext context) {
        UserDTO target = context.getMappedInstance( user, UserDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( user == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        context.storeMappedInstance( user, userDTO );

        if ( user.getId() != null ) {
            userDTO.setId( user.getId() );
        }
        userDTO.setUsername( user.getUsername() );
        userDTO.setEmail( user.getEmail() );
        userDTO.setPostLikeCount( user.getPostLikeCount() );
        userDTO.setCommentLikeCount( user.getCommentLikeCount() );
        userDTO.setPosts( postSetToPostDTOSet( user.getPosts(), context ) );
        userDTO.setComments( commentSetToCommentDTOSet( user.getComments(), context ) );
        userDTO.setRoles( roleSetToRoleDTOSet( user.getRoles(), context ) );
        userDTO.setCreatedAt( user.getCreatedAt() );
        userDTO.setUpdatedAt( user.getUpdatedAt() );

        return userDTO;
    }

    protected Set<UserDTO> userSetToUserDTOSet(Set<User> set, CycleAvoidingMappingContext context) {
        Set<UserDTO> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<UserDTO> set1 = new HashSet<UserDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( User user : set ) {
            set1.add( userToUserDTO( user, context ) );
        }

        return set1;
    }

    protected RoleDTO roleToRoleDTO(Role role, CycleAvoidingMappingContext context) {
        RoleDTO target = context.getMappedInstance( role, RoleDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( role == null ) {
            return null;
        }

        RoleDTO roleDTO = new RoleDTO();

        context.storeMappedInstance( role, roleDTO );

        roleDTO.setRole( role.getRole() );
        roleDTO.setUsers( userSetToUserDTOSet( role.getUsers(), context ) );

        return roleDTO;
    }
}
