package edu.rit.croatia.mapper;

import edu.rit.croatia.domain.Comment;
import edu.rit.croatia.domain.User;
import edu.rit.croatia.dto.CommentDTO;
import edu.rit.croatia.dto.UserSummaryDTO;
import edu.rit.croatia.mapper.config.context.CycleAvoidingMappingContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import javax.enterprise.context.ApplicationScoped;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-21T22:20:15+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 13.0.2 (AdoptOpenJDK)"
)
@ApplicationScoped
public class CommentMapperImpl implements CommentMapper {

    @Override
    public List<Comment> toDomain(List<CommentDTO> dto, CycleAvoidingMappingContext context) {
        List<Comment> target = context.getMappedInstance( dto, List.class );
        if ( target != null ) {
            return target;
        }

        if ( dto == null ) {
            return null;
        }

        List<Comment> list = new ArrayList<Comment>( dto.size() );
        context.storeMappedInstance( dto, list );

        for ( CommentDTO commentDTO : dto ) {
            list.add( toDomain( commentDTO, context ) );
        }

        return list;
    }

    @Override
    public Comment toDomain(CommentDTO dto, CycleAvoidingMappingContext context) {
        Comment target = context.getMappedInstance( dto, Comment.class );
        if ( target != null ) {
            return target;
        }

        if ( dto == null ) {
            return null;
        }

        Comment comment = new Comment();

        context.storeMappedInstance( dto, comment );

        comment.setId( dto.getId() );
        comment.setParentId( dto.getParentId() );
        comment.setBody( dto.getBody() );
        comment.setDeleted( dto.isDeleted() );
        comment.setCreatedAt( dto.getCreatedAt() );
        comment.setUpdatedAt( dto.getUpdatedAt() );
        comment.setComments( commentDTOSetToCommentSet( dto.getComments(), context ) );
        comment.setCommentLikes( dto.getCommentLikes() );
        comment.setUser( userSummaryDTOToUser( dto.getUser(), context ) );

        return comment;
    }

    @Override
    public List<CommentDTO> toListResource(List<Comment> entities, CycleAvoidingMappingContext context) {
        List<CommentDTO> target = context.getMappedInstance( entities, List.class );
        if ( target != null ) {
            return target;
        }

        if ( entities == null ) {
            return null;
        }

        List<CommentDTO> list = new ArrayList<CommentDTO>( entities.size() );
        context.storeMappedInstance( entities, list );

        for ( Comment comment : entities ) {
            list.add( toResource( comment, context ) );
        }

        return list;
    }

    @Override
    public CommentDTO toResource(Comment entity, CycleAvoidingMappingContext context) {
        CommentDTO target = context.getMappedInstance( entity, CommentDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( entity == null ) {
            return null;
        }

        CommentDTO commentDTO = new CommentDTO();

        context.storeMappedInstance( entity, commentDTO );

        if ( entity.getId() != null ) {
            commentDTO.setId( entity.getId() );
        }
        commentDTO.setBody( entity.getBody() );
        commentDTO.setDeleted( entity.isDeleted() );
        if ( entity.getParentId() != null ) {
            commentDTO.setParentId( entity.getParentId() );
        }
        commentDTO.setCommentLikes( entity.getCommentLikes() );
        commentDTO.setComments( commentSetToCommentDTOSet( entity.getComments(), context ) );
        commentDTO.setUser( userToUserSummaryDTO( entity.getUser(), context ) );
        commentDTO.setCreatedAt( entity.getCreatedAt() );
        commentDTO.setUpdatedAt( entity.getUpdatedAt() );

        return commentDTO;
    }

    protected Set<Comment> commentDTOSetToCommentSet(Set<CommentDTO> set, CycleAvoidingMappingContext context) {
        Set<Comment> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<Comment> set1 = new HashSet<Comment>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( CommentDTO commentDTO : set ) {
            set1.add( toDomain( commentDTO, context ) );
        }

        return set1;
    }

    protected User userSummaryDTOToUser(UserSummaryDTO userSummaryDTO, CycleAvoidingMappingContext context) {
        User target = context.getMappedInstance( userSummaryDTO, User.class );
        if ( target != null ) {
            return target;
        }

        if ( userSummaryDTO == null ) {
            return null;
        }

        User user = new User();

        context.storeMappedInstance( userSummaryDTO, user );

        user.setId( userSummaryDTO.getId() );
        user.setUsername( userSummaryDTO.getUsername() );

        return user;
    }

    protected Set<CommentDTO> commentSetToCommentDTOSet(Set<Comment> set, CycleAvoidingMappingContext context) {
        Set<CommentDTO> target = context.getMappedInstance( set, Set.class );
        if ( target != null ) {
            return target;
        }

        if ( set == null ) {
            return null;
        }

        Set<CommentDTO> set1 = new HashSet<CommentDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        context.storeMappedInstance( set, set1 );

        for ( Comment comment : set ) {
            set1.add( commentToCommentDTO( comment, context ) );
        }

        return set1;
    }

    protected UserSummaryDTO userToUserSummaryDTO(User user, CycleAvoidingMappingContext context) {
        UserSummaryDTO target = context.getMappedInstance( user, UserSummaryDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( user == null ) {
            return null;
        }

        UserSummaryDTO userSummaryDTO = new UserSummaryDTO();

        context.storeMappedInstance( user, userSummaryDTO );

        if ( user.getId() != null ) {
            userSummaryDTO.setId( user.getId() );
        }
        userSummaryDTO.setUsername( user.getUsername() );

        return userSummaryDTO;
    }

    protected CommentDTO commentToCommentDTO(Comment comment, CycleAvoidingMappingContext context) {
        CommentDTO target = context.getMappedInstance( comment, CommentDTO.class );
        if ( target != null ) {
            return target;
        }

        if ( comment == null ) {
            return null;
        }

        CommentDTO commentDTO = new CommentDTO();

        context.storeMappedInstance( comment, commentDTO );

        if ( comment.getId() != null ) {
            commentDTO.setId( comment.getId() );
        }
        commentDTO.setBody( comment.getBody() );
        commentDTO.setDeleted( comment.isDeleted() );
        if ( comment.getParentId() != null ) {
            commentDTO.setParentId( comment.getParentId() );
        }
        commentDTO.setCommentLikes( comment.getCommentLikes() );
        commentDTO.setComments( commentSetToCommentDTOSet( comment.getComments(), context ) );
        commentDTO.setUser( userToUserSummaryDTO( comment.getUser(), context ) );
        commentDTO.setCreatedAt( comment.getCreatedAt() );
        commentDTO.setUpdatedAt( comment.getUpdatedAt() );

        return commentDTO;
    }
}
