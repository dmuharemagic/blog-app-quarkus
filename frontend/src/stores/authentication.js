import { writable, get } from 'svelte/store';
import { writable as writableStorage } from './localStorageStore.js';
import decode from 'jwt-decode';
import axios from 'axios';

export const user = writableStorage('user', null);
export const identifier = writable(null);
export const groups = writable(null);

export const api = axios.create({ baseURL: 'http://localhost:8080/api/v1' });

if (get(user)) {
	api.defaults.headers.common['Authorization'] = `Bearer ${get(user).token}`;
	const decoded = decode(get(user).token);
	identifier.set({ id: decoded.userId });
	groups.set(decoded.groups);
} else {
	delete api.defaults.headers.common['Authorization'];
}

export async function login(username, password) {
	try {
		const response = await api({
			method: 'POST',
			url: '/auth/login',
			data: {
				username,
				password,
			},
		});

		if (response.statusText === 'OK') {
			const decoded = decode(response.data);
			user.set({ username: decoded.upn, token: response.data });
			identifier.set({ id: decoded.userId });
			groups.set(decoded.groups);
			api.defaults.headers.common['Authorization'] = `Bearer ${get(user).token}`;
		}
	} catch (e) {
		throw new Error(e.response.data.message);
	}
}

export async function logout() {
	user.set(null);
	identifier.set(null);
	groups.set(null);
	delete axios.defaults.headers.common['Authorization'];
}

export async function register(username, email, password) {
	try {
		await api({
			method: 'POST',
			url: '/auth/register',
			data: {
				username,
				email,
				password,
			},
		});
	} catch (e) {
		throw { data: e.response.data.errors || e.response.data.message };
	}
}

async function refreshToken() {
	try {
		const response = await api({
			url: '/api/v1/auth/token/refresh',
		});

		if (response.statusText === 'OK') {
			user.set({ username, token: response.data });
		}
	} catch (e) {
		throw { data: e.response.data.message };
	}
}
