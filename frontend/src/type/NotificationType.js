const NotificationType = Object.freeze({
	SUCCESS: 1,
	ERROR: 2,
});

export default NotificationType;
