module.exports = {
	theme: {
		extend: {
			fontSize: { '2xs': '.6875rem' },
		},
	},
	variants: {},
	plugins: [],
};
