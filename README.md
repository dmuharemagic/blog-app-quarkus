# Blog application

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

### Prerequisites

To be able to run the project, you must install the following:

* [Apache Maven](https://maven.apache.org/), a software project management and comprehension tool based on the concept of a project object model (POM)
* [Node](https://nodejs.org/en/), JavaScript runtime built on Chrome's V8 JavaScript engine

***To have a barebones structure of the database and insert dummy data, please run the `dump.sql` file provided in the main directory.***

### Login details

The admin & user capabilities can be tested by logging in to the provided admin account by using the following credentials:

`user` for the username
`user` for the password

### Description

The backend folder features a REST API built on [Quarkus](https://quarkus.io/).

Firstly, you must edit the necessary properties to be able to connect to your (local or remote) database. 
Please go into `backend/src/main/resources` folder and edit the `application.properties` file:

```sh
quarkus.datasource.username=ENTER_YOUR_USERNAME_HERE
quarkus.datasource.password=ENTER_YOUR_PASSWORD_HERE
```

To start the development process of the backend, clone the repository to your local store and run the development server by executing the following commands:

```sh
$ cd backend
$ mvn compile quarkus:dev
```

Verify the deployment of the backend by navigating to your server address in your preferred browser.

```sh
$ localhost:8080
```

The API endpoints listing is built upon the [OpenAPI specification](https://swagger.io/specification/) and [Swagger](https://swagger.io/). By targeting the following URI in your browser, one can preview all of the endpoints which are exposed to the clients.

```sh
$ http://localhost:8080/swagger-ui/
```

The client side implementation can be found in the frontend folder, featuring a reactive UI powered by [Svelte](https://svelte.dev/).

To run the local development server, clone the repository and execute the following commands:

```sh
$ cd frontend
$ npm install
$ npm run dev
```

### License

### The MIT License (MIT)

Copyright © `2020`

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
